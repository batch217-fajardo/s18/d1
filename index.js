// console.log("Hello World");

// Functions 

// Parameters and arguments

/*function printInput(){
	let nickname = prompt("Enter your nickname");
	console.log("Hi, " + nickname);
}
printInput();*/

// This function has parameter and argument
function printName(name) {
	console.log("My name is " + name)
}
printName("Juana");
printName("Erica Mae Fajardo");

let sampleVariable = "Yui";
printName(sampleVariable);

function checkDivisibilityBy8(num){
	let remainder = num % 8
	console.log("The remainder of " + num + " is divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(32);

// Function as Arguments
function argumentFunction(){
	console.log("This function was passed as an argument before the message");
}

function invokeFunction (argumentFunction) {
	argumentFunction();
}
invokeFunction(argumentFunction);

// finding more info about a funcion
console.log(argumentFunction);


// Multiple parameters

function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName)
}
createFullName("Erica Mae", "Estares", "Fajardo");
createFullName("Erica Mae", "Estares", "");

// Multiple parameter using stored data in a variable
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

function printFullName(middleName, firstName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}
printFullName("Juan", "Dela", "Cruz");

// Return Statement

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This will not be listed in the console");

}
let completeName = returnFullName("Juan", "Dela", "Cruz");
console.log(completeName);

console.log(returnFullName(firstName, middleName, lastName));

function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
	console.log("This will not be displayed");
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

function printPlayerInfo(username, level, job){
	// console.log("Username: " + username);
	// console.log("Level: " + level);
	// console.log("Job: " + job);
	return "Username: " + username;
}

let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
console.log(user1);